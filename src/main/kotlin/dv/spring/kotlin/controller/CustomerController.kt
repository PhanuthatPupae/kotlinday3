package dv.spring.kotlin.controller

import dv.spring.kotlin.entity.UserStatus
import dv.spring.kotlin.entity.dto.CustomerDto
import dv.spring.kotlin.service.CustomerService
import dv.spring.kotlin.service.ShoppingCartService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.query.Param
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.transaction.Transactional

@RestController
class CustomerController {
    @Autowired
    lateinit var customerService: CustomerService
    @Autowired
    lateinit var shoppingCartService: ShoppingCartService


    @GetMapping("/customer")
    fun getCustomer(): ResponseEntity<Any> {
        val customers = customerService.getCustomers()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomerDto(customers))
    }

    @GetMapping("/customer/query")
    fun getCustomer(@RequestParam("name") name: String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByName(name))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/customer/customerQuery")
    fun getCustomersPartial(@RequestParam("name") name: String,
                            @RequestParam(value = "email", required = false) email: String?): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapCustomerDto(
                customerService.getCustomerByPartialNameAndEmail(name, name))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customerAddress")
    fun getCustomerByAddress(@RequestParam("province") name: String): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapCustomerDto(
                customerService.getCustomerByAddress(name))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/status")
    fun getCustomerByStatus(@RequestParam("status") status: UserStatus): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapCustomerDto(
                customerService.getCustomerByStatus(status))
        return ResponseEntity.ok(output)
    }


    @GetMapping("/customer/product")
    fun getCustomerByProduct(@RequestParam("product") name: String): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapCustomerProductDto(
                shoppingCartService.getCustomerByProduct(name))
        return ResponseEntity.ok(output)
    }

    @PostMapping("/customer")
    fun addCustomer(@RequestBody customerDto: CustomerDto)
            : ResponseEntity<Any> {
        val output = customerService.save(MapperUtil.INSTANCE.mapCustomerDto(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

//    @PostMapping("/customer/address/{defaultAddressId}")
//    fun addCustomer(@RequestBody customerDto: CustomerDto,
//                    @PathVariable defaultAddressId: Long)
//            : ResponseEntity<Any> {
//        val output = customerService.save(defaultAddressId,
//                MapperUtil.INSTANCE.mapCustomerDto(customerDto))
//        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
//        outputDto?.let { return ResponseEntity.ok(it) }
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
//    }

    @DeleteMapping("/customer/{id}")
    fun deleteCustomer(@PathVariable("id") id:Long):ResponseEntity<Any>{
        val customer = customerService.remove(id)
        val outputCustomer = MapperUtil.INSTANCE.mapCustomerDto(customer)
        outputCustomer?.let{ return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the product id is not found")
    }


}