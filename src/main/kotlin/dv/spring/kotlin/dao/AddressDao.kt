package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Address
import dv.spring.kotlin.entity.dto.AddressDto


interface AddressDao{
    fun getAddresses(): List<Address>
    fun save(address: Address): Address
    fun findById(id: Long): Address?

}
