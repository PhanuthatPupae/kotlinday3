package dv.spring.kotlin.service

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.Product
import dv.spring.kotlin.entity.UserStatus

interface CustomerService {
    fun getCustomers(): List<Customer>
    fun getCustomerByName(name: String): Customer?
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer>
    fun getCustomerByAddress(name: String): List<Customer>
    fun getCustomerByStatus(status: UserStatus): List<Customer>
    fun save(customer: Customer): Customer
    fun remove(id: Long): Customer?
//    fun save(defaultAddressId: Long, customer: Customer): Customer

}