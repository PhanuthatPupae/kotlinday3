package dv.spring.kotlin.service

import dv.spring.kotlin.dao.AddressDao
import dv.spring.kotlin.dao.CustomerDao
import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.Product
import dv.spring.kotlin.entity.UserStatus
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class CustomerServiceImpl : CustomerService {
    @Transactional
    override fun remove(id: Long): Customer? {
        val customer = customerDao.findById(id)
        customer?.isDeleted = true
        return customer
    }

    @Transactional
    override fun save(customer: Customer): Customer {
        val address = customer.defaultAddress?.let{addressDao.save(it)}
        val customer = customerDao.save(customer)
        customer.defaultAddress = address
        return customer
    }

//    @Transactional
//    override fun save(defaultAddressId: Long, customer: Customer): Customer {
//        val address  = addressDao.findById(defaultAddressId)
//        val customer = customerDao.save(customer)
//        customer.defaultAddress = address
//        return customer
//    }

    @Autowired
    lateinit var customerDao: CustomerDao

    @Autowired
    lateinit var addressDao: AddressDao

    override fun getCustomerByStatus(status: UserStatus): List<Customer> {
        return customerDao.getCustomerByStatus(status)
    }

    override fun getCustomerByAddress(name: String): List<Customer> {
        return customerDao.getCustomerByAddress(name)
    }

    override fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer> {
        return customerDao.getCustomerByPartialNameAndDesc(name,email)
    }

    override fun getCustomerByPartialName(name: String): List<Customer> {
       return customerDao.getCustomerByPartialName(name)
    }

    override fun getCustomerByName(name: String): Customer?
        = customerDao.getCustomerByName(name)

    override fun getCustomers(): List<Customer> {
        return customerDao.getCustomers()
    }
}