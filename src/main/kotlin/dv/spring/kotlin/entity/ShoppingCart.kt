package dv.spring.kotlin.entity

import javax.persistence.*

@Entity
data class ShoppingCart(var shoppingCartStatus: ShoppingCartStatus = ShoppingCartStatus.WAIT) {
    @Id
    @GeneratedValue
    var id: Long? = null
    @OneToMany
    var selectedProduct = mutableListOf<SelectedProduct>()
    //    @OneToOne
    //    lateinit var Shipping: Address
    @OneToOne
    lateinit var customer: Customer
}